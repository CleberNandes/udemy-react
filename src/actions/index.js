import lodash from 'lodash'

import jsonPlaceholder from '../api/jsonPlaceholder';

export const selectSong = song => {
    return {
        type: 'SELECT_SONG',
        payload: song
    };
};

export const fetchPostsAndUsers = () => async (dispatch, getState) => {

    await dispatch(fetchPosts())
    // const usersIds = lodash.uniq(lodash.map(getState().posts, 'userId'))
    // usersIds.forEach(id => dispatch(fetchUser(id)))
    lodash
        .chain(getState().posts)
        .map('userId')
        .uniq()
        .forEach(id => dispatch(fetchUser(id)))
        .value()
};

export const fetchPosts = () => async dispatch => {

    const promissePosts = await jsonPlaceholder.get('/posts');
    dispatch({
        type: 'FETCH_POSTS',
        payload: promissePosts.data
    });
};

export const fetchUser = (user_id) => async dispatch => {

    const promisseUser = await jsonPlaceholder.get(`/users/${user_id}`)
    dispatch({
        type: 'FETCH_USER',
        payload: promisseUser.data
    })
}

// export const fetchUser = (user_id) => dispatch => {
//     _fetchUser(user_id, dispatch);
// }

// const _fetchUser = lodash.memoize( async (user_id, dispatch) => {
//     const promisseUser = await jsonPlaceholder.get(`/users/${user_id}`)
//     dispatch({
//         type: 'FETCH_USER',
//         payload: promisseUser.data
//     })
// })
