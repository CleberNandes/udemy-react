import React from 'react';

export class SearchBar extends React.Component {

    state = {search: ''};

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.search);

    }

    render(){
        return (

            <form className="ui form" onSubmit={this.onFormSubmit}>
                <div className="field">
                    <label>Search Bar</label>
                    <input type="text" 
                        value={this.state.search} 
                        onChange={e => this.setState({search: e.target.value})}/>
                </div>
            </form>
        );
    }
}