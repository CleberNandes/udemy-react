import React from 'react';
import { Link } from 'react-router-dom';

export const CommentDetail = (props) => {
    return (
        <div className="ui container comments">
            <div className="comment">
                <Link to="/" className="avatar">
                    <img alt="avatar" src={props.image}/>
                </Link>
                <div className="content">
                    <Link to="/" className="author">
                        {props.author}
                    </Link>
                    <div className="metadata">
                        <span className="date">{props.timeAgo}</span>
                    </div>
                    <div className="text">{props.comment}!</div>
                </div>
            </div>
        </div>
    );
}