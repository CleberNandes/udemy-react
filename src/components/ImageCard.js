import React from 'react'

export default class ImageCard extends React.Component {

    constructor(props) {
        super(props);
        this.imageRef = React.createRef();
        this.state = { spans: 0 };
    }

    componentDidMount() {
        this.imageRef.current.addEventListener('load', this.setSpan);
        
    }
    
    setSpan = (e) => {
        const height = this.imageRef.current.clientHeight;
        const spans = Math.ceil( height / 10 + 1 );
        this.setState({ spans })
        console.log(e);
        
    }


    render () {
        const {urls, description, alt_description} = this.props.image;
        return (
            <div className="image-card" style={{ gridRowEnd: `span ${this.state.spans}`}}>
                <img ref={this.imageRef} src={urls.small} alt={description}/>

                <div className="image-content">
                    <h4>{alt_description}</h4>                        
                    <p>{description}</p>
                </div>
            </div>
        )
    }
}