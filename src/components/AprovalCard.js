import React from 'react';

export const ApprovalCard = (props) => {
    return (
        <div className="ui cards">
            <div className="card">
                <div className="content">
                    {props.children}
                </div>
                <div className="extra content">
                    <div className="ui two buttons">
                        <button className="ui basic green button">Approve</button>
                        <button className="ui basic red button">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    );
}