import React from 'react';
import VideoItem from './VideoItem';

export default class VideoList extends React.Component {

    render() {
        const { videos, onVideoSelected } = this.props;
        return (
            <div className="videos-list ui relaxed divided list">
                {videos.map(
                    video => { 
                        console.log(video.id.videoId);
                        
                        return <VideoItem 
                            key={video.id.videoId} 
                            video={video} 
                            onVideoSelected={onVideoSelected}/>
                })}  
            </div>
        );
    }
}