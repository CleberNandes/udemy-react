import React from 'react';
import '../css/SearchList.css';
import ImageCard from './ImageCard';


export class SearchList extends React.Component {

    render() {
        
        return (
            <div className="image-list">
                { this.props.list.map((image) => 
                        
                    <ImageCard key={image.id} image={image}/>
                )}

            </div>
        );
    }
} 