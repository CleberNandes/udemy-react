import React from 'react';

export default class VideoItem extends React.Component {


    render() {
        const { video, onVideoSelected } = this.props;
        return (
            <div className="video-item item" onClick={() => onVideoSelected(video)}>
                <img className="ui image" src={video.snippet.thumbnails.default.url} alt={video.snippet.title}/>
                <div className="content">
                    <h3 className="header">{video.snippet.title}</h3>
                    <p className="description">{video.snippet.description}</p>
                </div>
            </div>
        );
    }
}