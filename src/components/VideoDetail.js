import React from 'react';

export default class VideoDetail extends React.Component {

    render() {
        const { video } = this.props;
        const hasVideo = video ? (
            <div className="video-detail">
                <div className="ui embed">
                    <img src={video.snippet.thumbnails.high.url} alt={video.snippet.title}/>
                    <iframe 
                        title={video.snippet.title}
                        src={`https://www.youtube.com/embed/${video.id.videoId}`} 
                        frameBorder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowFullScreen/>
                </div>
                <div className="ui segment">
                    <h4 className="header">{video.snippet.title}</h4>
                    <p>{video.snippet.description}</p>
                </div>
            </div>

        ) : <div></div>;
        return hasVideo;
    }
}