import React from 'react';
import { Link } from 'react-router-dom';


export default class App extends React.Component {
    render() {
        return (
            <div className="ui bottom attached segment pushable" style={{ height: '100vh' }}>
                <div className="ui visible inverted left vertical sidebar menu">
                    <Link  className="item" to="/comments">
                        <i className="home icon"></i>
                        comments
                    </Link>
                    <Link className="item" to="/season">
                        <i className="block layout icon"></i>
                        Season
                    </Link>
                    <Link className="item" to="/search">
                        <i className="smile icon"></i>
                        Search
                    </Link>
                    <Link className="item" to="/video">
                        <i className="calendar icon"></i>
                        Videos
                    </Link>
                    <Link className="item" to="/song">
                        <i className="calendar icon"></i>
                        Sons
                    </Link>
                    <Link className="item" to="/post">
                        <i className="calendar icon"></i>
                        Posts
                    </Link>
                </div>
                <div className="pusher">
                    <div className="ui basic segment">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}