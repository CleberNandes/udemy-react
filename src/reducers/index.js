import { combineReducers } from 'redux';
import { songListReducer, songSelectReducer } from './songReducers';
import postReducer from './postReducers';
import userReducer from './userReducer'


export default combineReducers({
    songList: songListReducer,
    songSelected: songSelectReducer,
    posts: postReducer,
    users: userReducer
})