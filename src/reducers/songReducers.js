export const songListReducer = () => {
  return [
      {
          title: 'whatwhever',
          duration: '4:04'
      },
      {
          title: 'whatwhever',
          duration: '2:34'
      },
      {
          title: 'whatwhever',
          duration: '3:40'
      },
      {
          title: 'whatwhever',
          duration: '2:14'
      },
      {
          title: 'whatwhever',
          duration: '3:32'
      },
  ]
}

export const songSelectReducer = (selectedSong = null, action) => {
  if(action.type === 'SELECT_SONG') {
      return action.payload;
  }

  return selectedSong;
}