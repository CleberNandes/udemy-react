export default (users = [], action) => {
  switch(action.type) {
    case 'FETCH_USER':
      if (!users.find(user => user.id === action.payload.id)){
        return [ ...users, action.payload]
      }
      return [ ...users ]
    default:
      return users;
  }

}
