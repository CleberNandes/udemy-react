import React from 'react'
import { SearchBar } from '../components/SearchBar';
import youtube from '../api/youtube';
import VideoDetail from '../components/VideoDetail';
import VideoList from '../components/VideosList';
import '../css/VideoPage.css';

export default class VideoPage extends React.Component {

    state = { videos: [], video: null};

    onSearchSubmit = async (term) => {
        console.log(term);
        
        const response = await youtube.get('/search', {
            params: { q: term },
        });

        console.log(response)
        this.setState({videos: response.data.items})
        console.log(this.state.videos);
        
    }
    onVideoSelected = (video) => {
        this.setState({video});
        console.log(video);
        
    }

    render() {
        const { video, videos } = this.state;
        return (
            <div className="ui container">
                <h1>Video Page</h1>
                <SearchBar onSubmit={this.onSearchSubmit}/>
                <div className="videos-content">

                    <VideoDetail video={video ? video : videos[0]}/>
                
                    <VideoList videos={videos} onVideoSelected={this.onVideoSelected}/>
                </div>
            </div>
        );
    }
}