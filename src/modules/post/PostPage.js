import React from 'react';
import PostList from './PostList';
import { connect } from 'react-redux';
// import jsonPlaceholder from '../../api/jsonPlaceholder';


export class PostPage extends React.Component {

  componentDidMount() {
    console.log(this.props);
    
  }

  render() {
    return (
      <div className="ui container">
        <PostList/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
}

export default connect(mapStateToProps)(PostPage);