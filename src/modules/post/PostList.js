import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchPostsAndUsers } from '../../actions';
import UserHeader from './UserHeader'

class PostList extends Component {

  componentDidMount() {
    console.log(this.props);
    this.props.fetchPostsAndUsers();
    
  }

  renderList() {
    return <div>
      {
        this.props.posts.map(post => {
          return (
            <div className="item" key={post.id}>
              <i className="large middle aligned icon user"></i>
              <div className="content">
                <div className="description">
                  <h4>{post.title}</h4>
                  <p>{post.body}</p>
                </div>
                <UserHeader user_id={post.userId}/>
              </div>
            </div>
          );
        })
      }
    </div>
  }

  render() {
    console.log(this.props);
    
    return (
      <div className="ui relaxed divided list">
        {this.renderList()}
      </div>
    )
  }
}

const mapStateToProps = state => {
  console.log(state);
  return { posts: state.posts };
}

export default connect(mapStateToProps, {
  fetchPostsAndUsers
})(PostList);
