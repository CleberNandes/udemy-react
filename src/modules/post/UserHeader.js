import React, { Component } from 'react'
import { connect } from 'react-redux'

class UserHeader extends Component {

  render() {
    if (!this.props.user) {
      return <div>Usuário não encontrado</div>
    }
    return <div className="header">{this.props.user.name}</div>
  }
}

const mapStateToProps = (state, propsUserHeader) => {
  return {user: state.users.find(user => user.id === propsUserHeader.user_id)}
}

export default connect(mapStateToProps)(UserHeader)
