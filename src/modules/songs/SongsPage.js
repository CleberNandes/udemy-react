import React from 'react';
import SongsList from './SongsList';
import './Songs.css';
import SongDetail from './SongDetail';

export default class SongsPage extends React.Component {
    render() {
        return (
            <div className="ui container grid songs-page">
                <div className="ui row">
                    <div className="column eight wide">
                        <SongsList/>
                    </div>
                    <div className="column four wide">
                        <SongDetail/>
                    </div>
                </div>
            </div>
        );
    }
}