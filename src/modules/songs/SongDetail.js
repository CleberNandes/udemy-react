import React from 'react';
import { connect } from 'react-redux'

class SongDetail extends React.Component {
    render() {
        if(this.props.songSelected) {
            return (
                <div className="song-detail">
                    <h3>{this.props.songSelected.title}</h3>
                    <h5>{this.props.songSelected.duration}</h5>
                </div>
            );
        } else {
            return <div></div>
        }
    }
}

const mapStateToProps = (state) => {
    return { songSelected: state.songSelected };
}

export default connect(mapStateToProps)(SongDetail);