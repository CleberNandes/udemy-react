import React from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../../actions';

class SongsList extends React.Component {

    renderList() {
        return this.props.songs.map(song => {
            return (
                <div key={song.duration} className="songs-item">
                    <h5 className="song-title">{song.title}</h5>
                    <button 
                        className="ui button primary"
                        onClick={() => this.props.selectSong(song)}>
                        Selecionar
                    </button>

                </div>
            );
        })
    }

    render() {
        return (
            <div className="ui divided list songs-list">
                {this.renderList()}
            </div>
        );
    }
    
}

const mapStateToProps = (state) => {
    return { songs: state.songList };
}

export default connect(mapStateToProps, {
    selectSong: selectSong
})(SongsList);