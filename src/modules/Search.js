import React from 'react';
import unsplash from '../api/unsplash';

import { SearchBar } from '../components/SearchBar';
import { SearchList } from '../components/SearchList';


export default class SearchPage extends React.Component {
    state = { images: []};

     onSearchSubmit = async (term) => {
        const response = await unsplash.get('/search/photos', {
            params: { query: term },
        });

        console.log(response.data.results)
        this.setState({images: response.data.results})
    }

    render() {
        return (
            <div className="ui" style={{marginTop: '10px'}}>
                <h1>Search Page</h1>
                <SearchBar onSubmit={this.onSearchSubmit}/>
                <SearchList list={this.state.images}/>
                Found: {this.state.images.length} images
            </div>

        );
    }
}