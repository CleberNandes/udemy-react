import React from 'react';

export default class Season extends React.Component {

    state = {location: '', error: ''};
    
    componentWillMount() {
        console.log('WillMount');
        window.navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({location: position.coords.latitude});
                console.log(position);
                
            },
            (error) =>this.setState({location: error.message})
        );
    }

    componentDidMount() {
        console.log('DidMount');
    }

    componentWillReceiveProps() {
        console.log('WillReceiveProps');
    }
    
    componentWillUpdate() {
        console.log('WillUpdate');
    }
    
    componentDidUpdate() {
        console.log('DidUpdate');
    }
    
    componentWillUnmount() {
        console.log('WillUnmount');
    }

    getSeason = (lat) => {
        const month = new Date().getMonth() + 1;
        
        if(month > 2 && month < 9){
            return lat > 0 ? 'summer' : 'winter';
        }
        return lat > 0 ? 'winter' : 'summer';
    }

    render() {
        console.log('render');
        if(this.state.location && !this.state.error){
            return (
                <div>
                    <h1>Position: { this.state.location }</h1>
                    <h3>{this.getSeason(this.state.latitude)}</h3>
                </div>
            );

        } else if(!this.state.location && this.state.error) {
            return <h1>error: { this.state.error }</h1>;     

        }
        return <h1>loading</h1>;            
    }
}