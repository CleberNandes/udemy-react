import React from 'react';
import { CommentDetail } from '../components/CommentDetail';
import { ApprovalCard } from '../components/AprovalCard';
import faker from 'faker';

export default class Comments extends React.Component {
    render() {
        return (
            <div>
                <ApprovalCard>
                    <h1>Hello</h1>
                </ApprovalCard>

                <ApprovalCard>
                    <CommentDetail 
                        author="Cleber" 
                        timeAgo="Today at 4:45PM" 
                        image={faker.image.avatar()} 
                        comment={faker.lorem.sentence()}/>
                </ApprovalCard>

                <ApprovalCard>
                    <CommentDetail 
                        author="Amanda" 
                        timeAgo="Yesterday at 4:45PM" 
                        image={faker.image.avatar()} 
                        comment={faker.lorem.sentence()}/>
                </ApprovalCard>

                <ApprovalCard>
                    <CommentDetail 
                        author="Melissa" 
                        timeAgo="Today at 4:45PM" 
                        image={faker.image.avatar()} 
                        comment={faker.lorem.sentence()}/>
                </ApprovalCard>
            </div>
        );
    }
}