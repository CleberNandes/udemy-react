import axios from 'axios';
const KEY = 'AIzaSyCgRnFhstbcbP46alci0iDPfZCGwOimHtE';

// Axios 400 "Required Parameter: part" Error
// In the upcoming lecture we will be creating an axios instance with default values to call the YouTube API with. As of today (5/30/2019), the axios library released v0.19.0 which seems to have included a bug that was never resolved in the beta version. This will cause a 400 error failure with the message "Required Parameter : part"

// The get around this and follow along with the course, do the following:

// npm install axios@0.18.1

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
})