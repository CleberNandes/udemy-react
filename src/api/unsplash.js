import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 
        'Client-ID eca5f59eb747ee7f0e4a1e410a8d6b2301d6113b7857ebdac13504facaeb9e8f'
    }
});