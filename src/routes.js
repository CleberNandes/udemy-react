import React from 'react';
import App from './template/App';
import {BrowserRouter as Router, Route, Switch, Redirect
} from 'react-router-dom';
import Comments from './modules/Comments';
import Season from './modules/Season';
import SearchPage from './modules/Search';
import VideoPage from './modules/VideoPage';
import SongsPage from './modules/songs/SongsPage';
import PostPage from './modules/post/PostPage';

import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';

import { hot } from 'react-hot-loader/root';

const store = createStore(reducers, applyMiddleware(thunk));

class CustomRoutes extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <App>
                        <Switch>
                            <Route exact path="/comments" component={Comments} />
                            <Route exact path="/season" component={Season} />
                            <Route exact path="/search" component={SearchPage} />
                            <Route exact path="/video" component={VideoPage} />
                            <Route exact path="/song" component={SongsPage} />
                            <Route exact path="/post" component={PostPage} />
                            <Redirect exact from='/' to='/comments'/>
                            <Redirect to='/post'/>
                        </Switch>
                    </App>
                </Router>
            </Provider>
        );
    }
}



export default hot(CustomRoutes);